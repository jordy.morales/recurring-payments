let passport = require('passport');
let bcrypt = require('bcrypt-as-promised');
let LocalStrategy = require('passport-local').Strategy;
Promise = require('bluebird');

// serializar
passport.serializeUser(function(user,done){
	done(null,user.id);
});

// desarializar
passport.deserializeUser(function(userID,done){
	User.findOne({id: userID})
			.then(user => done(null,user))
			.catch(err => done(err,false))
});


passport.use(new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password'
},function(email,password,done){

	let promiseFindUser = User.findOne({email: email});

	let promiseHashEquals = promiseFindUser.then(function(user){
		if(!user) return false;

		return bcrypt.compare(password,user.password);

	});

	Promise.all([promiseFindUser,promiseHashEquals])
				.spread(function(user,passwordCorrect){

					if(!user || !passwordCorrect)
						return done(null,false,{message: 'Usuario no encontrado o password incorrecto'});

					let returnUser = {
						email: user.email,
						id: user.id,
						createdAt: user.createdAt
					};

					return done(null,returnUser,{message:'Bienvenido.'})

				})
				.catch(err => done(err,false,{message:err}))

}));